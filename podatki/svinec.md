# Količina svinca

Podatki: [svinec.csv](svinec.csv)

## Opis

Meritve količine svinca v laseh na naslednjih vzorcih

1. vzorec 30 odraslih oseb, ki so umrle med letoma 1880 in 1920 in
2. vzorec 100 odraslih oseb, ki živijo danes.

## Format

Baza podatkov s 130 meritvami dveh spremenljivk

 1. *cas* je nominalna spremenljivka, ki ima dve vrednosti: P=preteklost, S=sedanjost.
 2. *kolicina* je numerična zvezna spremenljivka, ki predstavlja količino svinca v laseh (v
mikrogramih).


## Raziskovalna domneva
Količina svinca v laseh današnjih odraslih oseb je manjša.